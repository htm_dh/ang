import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../shared/services/products.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private products = [];


  constructor(private productsService:ProductsService) { }
 

  ngOnInit() {
    this.productsService.all().subscribe((response)=>{
      this.products = response['hits']['hits'];
    })
  }

}
