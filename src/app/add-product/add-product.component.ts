import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Key } from 'protractor';
import { ProductsService } from '../shared/services/products.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  private productForm: FormGroup;
  constructor(private productBuilder:FormBuilder, public ProductsService: ProductsService) { 

  }

  ngOnInit() {
    this.productForm=this.productBuilder.group({
      name : ['XX',Validators.required],
      price : ['0',Validators.required],
      description: ['..',Validators.required]
      
    })
  }
    addProduct(){
    this.ProductsService.add(this.productForm.value).subscribe((result: any)=>{

      console.log(result);
    })
    //let price=this.productForm.value.price
    //alert(price)

}
}
