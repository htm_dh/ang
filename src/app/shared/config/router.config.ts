import { HomeComponent } from 'src/app/home/home.component';
import { Routes } from '@angular/router';
import { AddProductComponent } from 'src/app/add-product/add-product.component';
import { ShowProductComponent } from 'src/app/show-product/show-product.component';



export const routes: Routes =
[
    {path: 'home',component: HomeComponent},
    {path: 'add-product',component: AddProductComponent},
    {path: 'show-product/:id',component:ShowProductComponent}

];