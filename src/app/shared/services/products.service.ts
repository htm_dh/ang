import { Injectable } from '@angular/core';
import { Product } from '../models/product';
import {HttpClient, HttpHeaders} from "@angular/common/http"

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
private products: Array<Product>;
header: HttpHeaders;
domaine: string;
  constructor(public http: HttpClient) {
    this.domaine= "";
    this.header = new HttpHeaders();
    this.header= this.header.append('Content-type','application/json');
    this.header=this.header.append('Accept','application/json');
   }
   all(){
     let url=this.domaine + '/epi/product/_search';
     let products = this.http.get(url,{headers:this.header});
     console.log(products);
     return products;
   }
   add(product){
     let url = this.domaine + "/epi/product";
     return this.http.post(url, product, {headers: this.header});
   }
   get(productId){
      let url=this.domaine+"/epi/product/"+productId;
      return this.http.get(url, {headers: this.header});
   }
   /*
  getProducts():any{
    this.products =[{
      id: 1,
      name:"Samsung galaxy s9",
      description: "une descp",
      image: "assets/img/dummyimg.png",
      price: 666523,
      category:"mobile",
      favorit:false


    },
  {
    id: 1,
    name:"Samsung galaxy s11",
    description: "une descp",
    image: "assets/img/dummyimg.png",
    price:112323,
    category:"mobile",
    favorit:false  
  },
  {
    id: 1,
    name:"Samsung galaxy s26",
    description: "une descp",
    image: "assets/img/dummyimg.png",
    price:22232,
    category:"mobile",
    favorit:false  
  }];
  return this.products;
  }*/
}
