import { ElasticSearchProduct } from './ElasticSearchProduct';

export interface Product{
    _id : string;
    _source: ElasticSearchProduct;
}