export interface ElasticSearchProduct{
    name : string;
    price :number;
    description: string;
}