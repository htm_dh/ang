import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductsService } from '../shared/services/products.service';
import { Product } from '../shared/models/product';

@Component({
  selector: 'app-show-product',
  templateUrl: './show-product.component.html',
  styleUrls: ['./show-product.component.css']
})
export class ShowProductComponent implements OnInit {
id:string;
  prd:Product;
  show:boolean;
  constructor(public router:ActivatedRoute,public productService:ProductsService) { }

  ngOnInit() {
    this.show=true;
    this.id=this.router.snapshot.paramMap.get('id');
    this.productService.get(this.id).subscribe((result:Product)=>{
      this.prd=result;
    })
  }

}
